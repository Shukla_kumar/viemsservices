// Set up
var express  = require('express');
var app      = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var cors = require('cors');
var url = require('url');
var jwt = require('jsonwebtoken');
// var Otp  =   require("./Otp/db.js");             //for adding page
// Configuration1

// mongoose.connect('mongodb://nkMalani:nkMalani123@ec2-107-20-43-19.compute-1.amazonaws.com:27017/dummyDB',{
// mongoose.connect('mongodb://viemsAdmins:viemsAdmin123@ec2-13-59-168-111.us-east-2.compute.amazonaws.com:27017/dummyDB',{
mongoose.connect('mongodb://127.0.0.1:27017/viems',{
  useMongoClient: true,
  /* other options */
});
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());
app.use(cors());

app.use(function(req, res, next) {
    // var decoded = jwt.verify(req.body.token,'secret',function(err, decoded){
    //     if(err){
    //         return res.status(401).json({
    //             title: 'not authenticated',
    //             error: err
    //         })
    //     }

    // });  
   res.header("Access-Control-Allow-Origin", "*" );
//    res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});


// Models
 var userLogin = mongoose.model('userLogin', {
		username: String ,
		password: String,
		isAdmin  : String,
        ToDate: String,
        updatedDate :String,
		Location: String,
});

 var userFormData = mongoose.model('userFormData', {
        userId          :   String,
		travelDocument	:	Array,
		personalDetail	:	Array,
        travelhistory   :   Array,
        spouse          :   Array,
        father          :   Array,
        mother          :   Array,
        misc            :   Array,
        visaStatus      :   Array,
        cosStatus       :   Array,
});

	//get user login data 
     app.get('/api/userLogin', function(req, res) {

        console.log("fetching User");
        var url_parts = url.parse(req.url,true);
        var decoded = jwt.verify(url_parts.query.token,'secret');
        console.log(JSON.stringify(decoded));
       // use mongoose to get all Offers in the database
        userLogin.findOne({"username" : url_parts.query.userName},function(err, userLogin) {
           
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err){
               return res.json({title:"does not exit"})
            }               

            if(!userLogin){
                return res.json({title:"user does not exit"})
            }  

            if(!decoded.username){
                return res.json({
                    title: 'user not auth',
                })
            }
            // if(userLogin.password != url_parts.query.password){
            //     return res.json({title:"pass does not exit"})
            // }
            // var token = jwt.sign({"username":url_parts.query.userName},'secret',{expiresIn:7200});
            res.json({userLogin});
             // return all Offers in JSON format
        });
    });
    
    app.get('/auth/userLogin', function(req, res) {
        
        console.log("fetching User");
        var url_parts = url.parse(req.url,true);

        // use mongoose to get all Offers in the database
        userLogin.findOne({"username" : url_parts.query.userName},function(err, userLogin) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err){
                return res.json({title:"does not exit"})
            }               

            if(!userLogin){
                return res.json({title:"user does not exit"})
            }  

            if(userLogin.password != url_parts.query.password){
                return res.json({title:"pass does not exit"})
            }
            var token = jwt.sign({"username":url_parts.query.userName},'secret',{expiresIn:7200});
            res.json({userLogin,token});
                // return all Offers in JSON format
        });
    });

    //get user userFormData data 
     app.get('/api/userFormData', function(req, res) {
 
        var url_parts = url.parse(req.url,true);
        // console.log(url_parts.query.dashboard);
        var start = parseInt(url_parts.query.filterL);
        var skip = parseInt(url_parts.query.filterS);
        var decoded = jwt.verify(url_parts.query.token,'secret');
        console.log(decoded);
        
        //for existing user list
        if(url_parts.query.existingEmployee != null){
            userFormData.find({ "visaStatus.ApplicationStaus" : "N/A"},function(err, userFormData) {
                console.log(url_parts.query.filterL);    
                // console.log(userFormData);
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)

            if(!decoded.username){
                return res.json({
                    title: 'user not auth',
                })
            }

            res.json(userFormData); // return all Offers in JSON format
            }).limit(start).skip(skip);
        }

        //for cos or visa form for admin 
        if(url_parts.query.userId != null){
            console.log(url_parts.query.userId);
                    //for cosstatus check
            if(url_parts.query.cosstatus != null){
                userFormData.findOne({ userId : url_parts.query.userId ,"cosStatus.openCloseFile" : url_parts.query.cosstatus },function(err, userFormData) {

                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                if (err)
                    res.send(err)

                if( !decoded.username){
                    return res.json({
                        title: 'user not auth',
                    })
                }

                res.json(userFormData); // return all Offers in JSON format
                });
            }
            else{
                userFormData.findOne({ userId : url_parts.query.userId },function(err, userFormData) {
                // userFormData.findOne({ userId : url_parts.query.userId },function(err, userFormData) {

                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                if (err)
                    res.send(err)
                if(!decoded.username){
                    return res.json({
                        title: 'user not auth',
                    })
                }
                res.json(userFormData); // return all Offers in JSON format
                });
                }
        }

        if(url_parts.query.dashboard != null){
            var today = new Date();
            var dayAdd = 7;
            var next = 21;
            var t = new Date();
            var td = new Date();
            var tdf = new Date();

            t.setDate(t.getDate() + dayAdd);
            td.setDate(td.getDate() - dayAdd);
            tdf.setDate(tdf.getDate() + next);          
        
            this.dateNextWeek = t.getFullYear()+"-"+(((t.getMonth() + 1) < 10 ) ? '0'+(t.getMonth()+1):(t.getMonth()+1))+"-"+ ((t.getDate() < 10) ?  '0'+t.getDate() : t.getDate())
            //this.datePreviousWeek =td.getFullYear()+"-"+(((td.getMonth() + 1) < 10 ) ? '0'+(td.getMonth()+1) : (td.getMonth()+1))+"-"+((td.getDate() < 10) ?  '0'+td.getDate() : td.getDate());
            this.date14after  = tdf.getFullYear()+"-"+(((tdf.getMonth() + 1) < 10 ) ? '0'+(tdf.getMonth()+1) : (tdf.getMonth()+1))+"-"+((tdf.getDate() < 10) ?  '0'+tdf.getDate() : tdf.getDate());
            console.log(this.dateNextWeek);
           // console.log(this.datePreviousWeek);
            console.log(this.date14after);
            var newDate = new Date().toISOString();
            //console.log(today);
            this.dateToMatch = newDate.substring(0,10);
            console.log(this.dateToMatch);
            // console.log(date);
            // userFormData.find({"cosStatus.cosExpiryDate": { "$gte" : this.dateToMatch , "$lte" : this.date14after}},
            userFormData.find({"cosStatus.cosExpiryDate": { "$gte" : this.dateToMatch , "$lte" : this.date14after}},function(err, userFormData) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)

            if(!decoded.username){
                return res.json({
                    title: 'user not auth',
                })
            }

            res.json(userFormData); // return all Offers in JSON format
            }).limit(start).skip(skip);
        }
    });


	//create user login data
     app.post('/api/userLogin', function(req, res) {
        console.log("creating Offer");
        // create a Request, information comes from request from Ionic
        userLogin.create({
            username: req.body.username,
			password: req.body.password,
			isAdmin : req.body.isAdmin,
			// ToDate: req.body.ToDate,
			// Location: req.body.Location,
			// budgetAmount: req.body.budgetAmount,
			// productcondition: req.body.productcondition,
			// deliverycondition: req.body.deliverycondition,
			// FirstName:  req.body.FirstName,
			// MobileNo:	req.body.MobileNo
        }, function(err, formSubmit) {
            if (err)
                res.send(err);

            // get and return all the Offers after you create another
            userLogin.find(function(err, formSubmit) {
                if (err)
                    res.send(err)
                res.json(formSubmit);
            });
        });

        //sending email using postmark
        // var postmark = require("postmark");
        // var client = new postmark.Client("48821b9b-1740-4c7d-9850-b81a90ed5efa");
        // var fs = require('fs');
        
        // client.sendEmail({
            // "From": "connect@yBuySell.com", 
            // "To": req.body.username, 
            // "Subject": "Visa application login", 
            // "TextBody": "Your UserID is "+ req.body.username +" & your password is " + req.body.password,
            // // "Attachments": [{
            // // // Reading synchronously here to condense code snippet: 
            // // "Content": fs.readFileSync("./unicorns.jpg").toString('base64'),
            // // "Name": "PrettyUnicorn.jpg",
            // // "ContentType": "image/jpeg"
            // // }]
        // }, function(error, result) {
            // if(error) {
                // console.error("Unable to send via postmark: " + error.message);
                // return;
            // }
            // console.info("Sent to postmark for delivery")
        // });
    });
	
	//cerate collection for inserting user data
	 app.post('/api/userFormData', function(req, res) {
        console.log("creating Offer");
        // create a Request, information comes from request from Ionic
        userFormData.create({
            // travelDocument: req.body.travelDocument,
            userId: req.body.userId,
            visaStatus : req.body.visaStatus
        }, function(err, formSubmit) {
            if (err)
                res.send(err);

            // get and return all the Offers after you create another
            userFormData.find(function(err, formSubmit) {
                if (err)
                    res.send(err)
                res.json(formSubmit);
            });
        });

    });

	//sending new password request
     app.post('/api/newPassword', function(req, res) {      
        console.log("new password");
        //sending email using postmark
        var postmark = require("postmark");
        var client = new postmark.Client("48821b9b-1740-4c7d-9850-b81a90ed5efa");
        var fs = require('fs');
        
        client.sendEmail({
            "From": "connect@yBuySell.com", 
            "To": "shukla.amit2222@gmail.com", 
            "Subject": "Visa application login", 
            "TextBody": "Your UserID is "+ req.body.userName,
            // "Attachments": [{
            // // Reading synchronously here to condense code snippet: 
            // "Content": fs.readFileSync("./unicorns.jpg").toString('base64'),
            // "Name": "PrettyUnicorn.jpg",
            // "ContentType": "image/jpeg"
            // }]
        }, function(error, result) {
            if(error) {
                console.error("Unable to send via postmark: " + error.message);
                return;
            }
            console.info("Sent to postmark for delivery")
        });
    });
	
    //api call for update in userFormData   
    app.put('/api/userFormData', function(req, res) {
        console.log(req.body.date);

        var travelDocument  =   req.body.travelDocument;
        var personalDetail  =   req.body.personal;
        var travelhistory   =   req.body.travelhistory;
        var spouse          =   req.body.spouse;
        var father          =   req.body.father;
        var mother          =   req.body.mother;
        var misc            =   req.body.misc;
        var visaStatus      =   req.body.visaStatus;
        var queryStatusUpdate = req.body.date;
        var cosStatus         = req.body.cosStatus;

        //upate query for travelDocument page
        if(travelDocument != null){
            
            try {
            userFormData.update(
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { userId   :   req.body.id},
                {				
                    travelDocument : req.body.travelDocument
                    // travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userFormData.find({userId:  req.body.id},function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
            }
        }
        //upate query for personalDetail page
        if(personalDetail != null){

            try {
            userFormData.update(
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { userId   :   req.body.id},
                {				
                    personalDetail : req.body.personal
                    // travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userFormData.find({userId:  req.body.id},function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
            }
        }

        //upate query for travelhistory page
        if(travelhistory != null){

            try {
            userFormData.update(
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { userId   :   req.body.id},
                {				
                    // personalDetail : req.body.personal,
                    travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userFormData.find({userId:  req.body.id},function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
            }
        }
       
        //upate query for spouse page
        if(spouse != null){

            try {
            userFormData.update(
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { userId   :   req.body.id},
                {				
                    spouse : req.body.spouse
                    // travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userFormData.find({userId:  req.body.id},function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
            }
        }

        //upate query for father page
        if(father != null){

            try {
            userFormData.update(
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { userId   :   req.body.id},
                {				
                    father : req.body.father
                    // travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userFormData.find({userId:  req.body.id},function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
            }
        }

        //upate query for mother page
        if(mother != null){

            try {
            userFormData.update(
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { userId   :   req.body.id},
                {				
                    mother : req.body.mother
                    // travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userFormData.find({userId:  req.body.id},function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
            }
        }

        //upate query for misc page
        if(misc != null){

            try {
            userFormData.update(
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { userId   :   req.body.id},
                {				
                    misc : req.body.misc
                    // travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userFormData.find({userId:  req.body.id},function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
            }
        }

        if(cosStatus != null){

            try {
            userFormData.update(
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { userId   :   req.body.id},
                {				
                    cosStatus : req.body.cosStatus
                    // travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userFormData.find({userId:  req.body.id},function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
            }
        }

        //upate query for visaStatus page
        if(visaStatus != null){

            try {
            userFormData.update( 
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { userId   :   req.body.id},
                {				
                    visaStatus : req.body.visaStatus
                    // travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userFormData.find({userId:  req.body.id},function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
            }
        }

        if(queryStatusUpdate != null){
            try {
                 userFormData.update( 
                /*  { MobileNo :"432533"},                      //query to find & update
                    {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                    {
                        // userId :"nkmalani"                        
                        // "visaStatus.visaStartDate" :  { "$gt" : req.body.date },
                        // "visaStatus.visaEndDate"   :  { "$lte" : "2017-07-31" }
                         "visaStatus.visaStartDate"   : { "$lte" : req.body.date },
                         "visaStatus.visaEndDate"   : { "$gte" : req.body.date }
                       
                    },
                    {	
                        // userId:"nkmalani@gmai.com"			
                          "visaStatus.VisaStatus" : "Valid" 
                        // travelhistory  : req.body.travelhistory
                    },
                    {
                        multi: true,
                    },
                    function(err, U) {
                if (err)
                    res.send(err);
            
                // userFormData.find(function(err, newValue) {
                //     if (err)
                //         res.send(err)
                //     res.json(newValue);
                // });

                    });

                } catch (e) {
                console.log("error"+ e);
                }
        }


        if(queryStatusUpdate != null){
            console.log("Expires");
            try {
                 userFormData.update( 
                /*  { MobileNo :"432533"},                      //query to find & update
                    {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                    {
                        // userId :"nkmalani"
                        // "visaStatus.ApplicationStaus"   : "Approved",                        
                        // "visaStatus.visaEndDate"   :  { "$lte" : "2017-07-31" },
                        "visaStatus.visaEndDate" :  { "$lt" : req.body.date }
                        
                    },
                    {	
                        // userId:"nkmalani@gmai.com"			
                          "visaStatus.VisaStatus" : "Expires" 
                        // travelhistory  : req.body.travelhistory
                    },
                    {
                        multi: true,
                    },
                    function(err, U) {
                if (err)
                    res.send(err);
            
                // userFormData.find(function(err, newValue) {
                //     if (err)
                //         res.send(err)
                //     res.json(newValue);
                // });

                    });

                } catch (e) {
                console.log("error"+ e);
                }
        }

        
        if(queryStatusUpdate != null){
            
            try {
                 userFormData.update( 
                /*  { MobileNo :"432533"},                      //query to find & update
                    {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                    {
                        // userId :"nkmalani"
                        "visaStatus.ApplicationStaus"   : "Approved",                        
                        "visaStatus.departureDate" :  { "$gt" : req.body.date },
                        // "visaStatus.visaEndDate"   :  { "$lte" : "2017-07-31" }
                    },
                    {	
                        // userId:"nkmalani@gmai.com"			
                          "visaStatus.TravelDocument" : "Not Arrived" 
                        // travelhistory  : req.body.travelhistory
                    },
                    {
                        multi: true,
                    },
                    function(err, U) {
                if (err)
                    res.send(err);
            
                // userFormData.find(function(err, newValue) {
                //     if (err)
                //         res.send(err)
                //     res.json(newValue);
                // });

                    });

                } catch (e) {
                console.log("error"+ e);
                }
        }

        if(queryStatusUpdate != null){
            
            try {
                 userFormData.update( 
                /*  { MobileNo :"432533"},                      //query to find & update
                    {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                    {
                        // userId :"nkmalani"
                        "visaStatus.ApplicationStaus"   : "Approved",                        
                        "visaStatus.departureDate" :  { "$lte" : req.body.date },
                        // "visaStatus.visaEndDate"   :  { "$lte" : "2017-07-31" }
                    },
                    {	
                        // userId:"nkmalani@gmai.com"			
                          "visaStatus.TravelDocument" : "Arrived" 
                        // travelhistory  : req.body.travelhistory
                    },
                    {
                        multi: true,
                    },
                    function(err, U) {
                if (err)
                    res.send(err);
            
                // userFormData.find(function(err, newValue) {
                //     if (err)
                //         res.send(err)
                //     res.json(newValue);
                // });

                    });

                } catch (e) {
                console.log("error"+ e);
                }
        }

        if(queryStatusUpdate != null){
            
            try {
                 userFormData.update( 
                /*  { MobileNo :"432533"},                      //query to find & update
                    {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                    {
                        // userId :"nkmalani"
                        "visaStatus.ApplicationStaus"   : "Approved",                        
                        "visaStatus.outDepartureDate" :  { "$lt" : req.body.date },
                        // "visaStatus.visaEndDate"   :  { "$lte" : "2017-07-31" }
                    },
                    {	
                        // userId:"nkmalani@gmai.com"			
                          "visaStatus.TravelDocument" : "Returned" 
                        // travelhistory  : req.body.travelhistory
                    },
                    {
                        multi: true,
                    },
                    function(err, U) {
                if (err)
                    res.send(err);
            
                // userFormData.find(function(err, newValue) {
                //     if (err)
                //         res.send(err)
                //     res.json(newValue);
                // });

                    });

                } catch (e) {
                console.log("error"+ e);
                }
        }       

     });

     //update 
    app.put('/api/userLogin', function(req, res) {
        //console.log(req.body.date);
        // create a Request, information comes from request from Ionic
        try {
            userLogin.update(
              /*  { MobileNo :"432533"},                      //query to find & update
                {FirstName : "Ajitk"},{multi:true},         //set to Update  */
                { username   :   req.body.username},
                {				
                    password : req.body.password ? req.body.password : req.body.newPassword
                    // travelhistory  : req.body.travelhistory
				},
                function(err, U) {
            if (err)
                res.send(err);
           
            userLogin.find(function(err, newValue) {
                if (err)
                    res.send(err)
                res.json(newValue);
            });

                });

            } catch (e) {
            console.log("error"+ e);
        }

        //sending email using postmark     //08/08/2017
        // if (req.body.password){

        //     var postmark = require("postmark");
        //     var client = new postmark.Client("48821b9b-1740-4c7d-9850-b81a90ed5efa");
        //     var fs = require('fs');
            
        //     client.sendEmail({
        //         "From": "connect@yBuySell.com", 
        //         "To": req.body.username, 
        //         "Subject": "Visa application login", 
        //         "TextBody": "Your UserID is "+ req.body.username +" & your password is " + req.body.password,
        //         // "Attachments": [{
        //         // // Reading synchronously here to condense code snippet: 
        //         // "Content": fs.readFileSync("./unicorns.jpg").toString('base64'),
        //         // "Name": "PrettyUnicorn.jpg",
        //         // "ContentType": "image/jpeg"
        //         // }]
        //     }, function(error, result) {
        //         if(error) {
        //             console.error("Unable to send via postmark: " + error.message);
        //             return;
        //         }
        //         console.info("Sent to postmark for delivery")
        //     });
        // }

         if(req.body.date){
             //console.log("check and test" + req.body.date);
            try {
                 userLogin.update( 
                    {                      
                        updatedDate : { "$lt" : req.body.date }             
                        //updatedDate : { "$lt" : req.body.date } 
                    },
                    {	
                        //isAdmin : "YES",
                        updatedDate : req.body.date
                        // userId:"nkmalani@gmai.com"	
                    },
                    {
                        multi: true,
                    },
                    function(err, U) {
                if (err)
                    res.send(err);
            
                // userFormData.find(function(err, newValue) {
                //     if (err)
                //         res.send(err)
                //     res.json(newValue);
                // });

                    });

                } catch (e) {
                console.log("error"+ e);
                }
        }
    });

app.listen(8000);
console.log("App listening on port 8000");